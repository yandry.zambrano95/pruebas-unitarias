package facci.yandrizambrano.tes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText correo, contraseña;
    private Button btn1;
    String usuario=" yandri11@gmail.com";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();
    }
    private void setViews() {

       btn1.setOnClickListener(this);
        btn1=findViewById(R.id.btn1);

        correo = findViewById(R.id.correo);
        contraseña=findViewById(R.id.contraseña);

    }

    private void Validacion( String correo, String contraseña){
        if(correo.isEmpty()|| contraseña.isEmpty())
        Toast.makeText(this, "Los campos estan vacios", Toast.LENGTH_SHORT).show();
        else Toast.makeText(this, "Datos correctos", Toast.LENGTH_SHORT).show();
    }
    private void validaCo( String correo){
        if ( correo== usuario)
            Toast.makeText(this, " correo exitoso", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "El correo no es valido", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onClick(View v) {


    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged(hasCapture);
    }
}